---
title: "Benchmarks for the cost per person of mass treatment against neglected tropical diseases"
runtime: shiny
output: html_document
---

This app provides benchmarks for the cost per person of delivering mass treatment campaigns, excluding the cost of medicines. For more information, see [Fitzpatrick et al. 2016](https://drive.google.com/open?id=0B8uhnL5tetUlWGlQa185LWt5WTg). 

On the left, set general, setting and advanced parameters. Default setting parameters are based on median values from the literature. On the right, view the resulting benchmark (with intervals) in a table or in a plot with unit costs from the literature. For more information, see the [user guide.](https://drive.google.com/open?id=0B8uhnL5tetUlOS1ISjVvYXY0MFU)

```{r housekeeping, echo=FALSE}
suppressPackageStartupMessages(library(plm))
suppressPackageStartupMessages(library(shiny))
suppressPackageStartupMessages(library(ggplot2))
options(scipen=100000)

load("benchmark.Rdata")
mda_uc_raw$id<-factor(c(as.factor(mda_uc_raw$study_ref)))
```

```{r ui, echo=FALSE}

pageWithSidebar(
  headerPanel(""),
  sidebarPanel(width = 5,
               tabsetPanel(
         tabPanel("General", h3(""),
    radioButtons("eco", "Financial or economic costs excluding volunteer time ", c("Financial" = 0, "Economic" = 1)),
        sliderInput("cir", "Confidence interval (CI)", min=0, max=100, value=c(2.5,97.5), step = 0.5, round = FALSE),
    sliderInput("pir", "Prediction interval (PI)", min=0, max=100, value=c(25,75), step = 0.5, round = FALSE)
    ),
       tabPanel("Setting", h3(""),
       numericInput("pop", "Population to be treated", round(median(mda_uc_raw$pop, na.rm=T),0), min = 1),
    sliderInput("cov", "Coverage rate", min=1, max=100, value= round(median(mda_uc_raw$cov, na.rm=T),0), step =   1, round = FALSE),
    radioButtons("nat", "National or subnational",c("National" = 1, "Subnational" = 0), selected = 0),    radioButtons("sch", "School-based delivery",c("Yes" = 1,"No" = 0), selected = 0),
    radioButtons("vol", "Volunteers used",c("Yes" = 1, "No" = 0), selected = 1),        
    sliderInput("int", "Number of diseases integrated", min=1, max=5, value=1),
    sliderInput("rds", "Number of rounds per year", min=1, max=3, value=1),
    sliderInput("yrs", "Year of implementation", min=1, max=10, value=3, step = 1, round = FALSE),
# numericInput("year", "Year", 2015, min = min(mda_uc_raw$year, na.rm=T)),
    numericInput("gdp", "GDP per capita (2015 US$)", round(median(mda_uc_raw$gdp, na.rm=T),0), min = 1),
    numericInput("den", "Population density (people per square km)", round(median(mda_uc_raw$den, na.rm=T),0), min = min(mda_uc_raw$den, na.rm=T), max=max(mda_uc_raw$den, na.rm=T)),
    #radioButtons("UGA", "Setting similar to Uganda",c("Yes" = 1,"No" = 0), selected = 0),
    radioButtons("VUT", "Small Island Developing State",c("Yes" = 1,"No" = 0), selected = 0)
  ),
 tabPanel("Advanced", h3(""),
      radioButtons("eff", "Random or fixed effects",c("Random" = 1, "Fixed" = 0), selected = 1),        
      selectInput("mth", "Study-specific fixed effect",unique(mda_uc_raw$study_ref), selected = "McFarland and Menzies 2005 (unpublished)")
    ))),
mainPanel(width = 7,
                   tabsetPanel(
tabPanel("Table", h3(""), tableOutput("estimate")),
tabPanel("Plot", plotOutput("plot", height="550px", width = "800px")),
tabPanel("Data", downloadButton('datadownload', 'Download all entries'),dataTableOutput("datatable"))
)
)
)
```

```{r server, echo=FALSE}

 data <- reactive({
    ucb<-as.data.frame(cbind(
    gdp=as.numeric(input$gdp),
    den=as.numeric(input$den),
    cov=as.numeric(input$cov),
    # yrn=as.numeric(input$year)-min(mda_uc_raw$year, na.rm=T),
    yrs=as.numeric(input$yrs),
    rds=as.numeric(input$rds),
    pop=as.numeric(input$pop)))
for (i in 1:max(as.numeric(mda_uc_raw$id))) {
  ifelse(i==mda_uc_raw$id[mda_uc_raw$study_ref==input$mth],ucb[,paste0("id",i)]<-1,ucb[,paste0("id",i)]<-0)
}
  ucb$log_gdp<-log(ucb$gdp)
  ucb$log_den<-log(ucb$den)
  ucb$log_pop<-log(ucb$pop)
  ucb$eco_vol<-as.numeric(input$eco)*as.numeric(input$vol)
  ucb$eco_int<-as.numeric(input$eco)*as.numeric(input$int)
  ucb$yrs_cov<-ucb$yrs*ucb$cov
  ucb$cov_sch<-as.numeric(input$cov)*as.numeric(input$sch)
  ucb$log_den_nat<-as.numeric(ucb$log_den)*as.numeric(input$nat)
  # ucb$log_pop_UGA<-as.numeric(ucb$log_pop)*as.numeric(input$UGA)
  ucb$eco <- as.numeric(input$eco)
  ucb$sch <- as.numeric(input$sch)
  ucb$int <- as.numeric(input$int)
  ucb$log_int<-log(ucb$int)
  ucb$eco_log_int<-ucb$eco*ucb$log_int
  ucb$vol <- as.numeric(input$vol)
  ucb$nat <- as.numeric(input$nat)
  #ucb$UGA <- as.numeric(input$UGA)
  ucb$VUT <- as.numeric(input$VUT)
  ucb$Kri<-0
ucb$Fri<-0
ucb$Mon<-0
  ucb$log_rds<-log(ucb$rds)
  ucb$cov_nat<-ucb$cov*ucb$nat
ucb$eco_sch<-ucb$eco*ucb$sch
ucb$nat_log_pop<-ucb$nat*ucb$log_pop
ucb$nat_log_den<-ucb$nat*ucb$log_den
ucb$nat_log_gdp<-ucb$nat*ucb$log_gdp
  ucb$intercept <- 1
  ifelse(input$eff==1,ucb_mat<-as.matrix(t(as.numeric(ucb[,varlist_re]))),ucb_mat<-as.matrix(t(as.numeric(ucb[,varlist_fe]))))
  eco<-input$eco
pop<-ucb$pop

ifelse(input$eff==1,model<-model1_re_usd,model<-model1_fe_du_usd)
ucb$xb<-t(t(as.matrix(coefficients(model)))%*%t(ucb_mat))
ucb$se<-sqrt(as.vector(diag(ucb_mat%*%as.matrix(vcov(model))%*%t(ucb_mat))))

set.seed(2)  
temp<-rnorm(10001,mean=ucb$xb,sd=ucb$se)
    ucb$ci_best <- round(mean(exp(temp)),2)
    ucb$ci_lo <- round(quantile(exp(temp), c(input$cir[1]/100)),2)
    ucb$ci_hi <- round(quantile(exp(temp), c(input$cir[2]/100)),2)

ucb$se<-sqrt(sum(residuals(model)^2)/(length(residuals(model)-2))+ucb_mat%*%as.matrix(vcov(model))%*%t(ucb_mat))

temp<-rnorm(10001,mean=ucb$xb,sd=ucb$se)
    ucb$pi_best <- format(mean(exp(temp)),digits=2,nsmall=2,justify=c("centre"))
    ucb$pi_lo <- round(quantile(exp(temp), c(input$pir[1]/100)),2)
    ucb$pi_hi <- round(quantile(exp(temp), c(input$pir[2]/100)),2)   

ucb$ci<-paste0(format(ucb$ci_lo,digits=2,nsmall=2),"-",format(ucb$ci_hi,digits=2,nsmall=2))
ucb$pi<-paste0(format(ucb$pi_lo,digits=2,nsmall=2),"-",format(ucb$pi_hi,digits=2,nsmall=2))
est<-cbind(ucb[,c("pi_best","ci","pi","ci_best","ci_lo","ci_hi","pi_lo","pi_hi")],eco,pop)
                   })

 output$estimate <- renderTable({
   est<-data() 
   colnames(est) <- c("Benchmark (2015 US$)", "CI","PI")
print(est[,1:3])
  })
  
output$plot <- renderPlot({
  est<-data()
  eco<-ifelse(est$eco==0,"uc_fin_ex_2015_us","uc_econ_ex_novol_2015_us")
  attach(mda_uc_raw)
    
p<-  ggplot(mda_uc_raw[is.na(get(eco))==FALSE,], aes(pop_treated, get(eco), colour=study_ref), environment=environment()) + geom_point() + ylab("Unit cost (US$), log-scale") + xlab("Population treated, log-scale") + labs(title = "") + scale_x_log10(breaks=c(500,1000,5000,10000,50000,100000,500000,1000000,3000000)) + scale_y_log10(breaks=c(0.01,0.05,0.10,0.20,0.30,0.50,1,2,3)) + geom_errorbar(aes(x=est$pop,y=est$ci_best), ymin=log10(est$ci_lo), ymax=log10(est$ci_hi), colour="black", size=1, width=0.25)   +      geom_errorbar(aes(x=est$pop,y=est$ci_best), ymin=log10(est$pi_lo), ymax=log10(est$pi_hi), colour="black", size=1,width=0.25) +
    annotation_logticks()+guides(col = guide_legend(ncol = 1)) + guides(colour=guide_legend(title="Reference"))

print(p)
  detach(mda_uc_raw)
  })

output$datatable <- renderDataTable({mda_uc[,c("study_ref","country","sau","year","ucb","eco","vol","int","rds","yrs","cov","sch","pop","den","nat","gdp","dis")]}, options = list(lengthMenu = c(5, 10, 25), pageLength = 5))

output$datadownload <- downloadHandler(
    filename = function() {paste('data-', Sys.Date(), '.csv', sep='')},
    content = function(file) {
    write.csv(mda_uc[,c("study_ref","country","sau","year","ucb","eco","vol","int","rds","yrs","cov","sch","pop","den","nat","gdp","dis")], file)
})

```




